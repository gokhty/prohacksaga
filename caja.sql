drop database caja;
create database caja;
use caja;
create table usuarios(
id int primary key auto_increment,
nombre varchar(45),
correo varchar(45),
dni char(8),
usu varchar(45),
pass varchar(45)
);

delimiter |
create procedure sp_regUsuario(pnom varchar(45), pcor varchar(45), pdni char(8), pusu varchar(45), ppass varchar(45))
begin
INSERT INTO usuarios(nombre,correo,dni,usu,pass)values
(pnom, pcor, pdni, pusu, ppass);
end
|
call sp_regUsuario('pnom', 'pcor', 'pdni', 'pusu', 'ppass');
select * from usuarios;

drop procedure sp_login;
delimiter |
create procedure sp_login(pusu varchar(45), ppass varchar(45))
begin
select count(*) as valida from usuarios where usu = pusu and pass = ppass;
end
|

call sp_login('adreim', '1d234');
select correo from usuarios limit 1